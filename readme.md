# UC Berkeley Course CS294-112 - Deep Reinforcement Learning (Fall 2018)


Lecture notes, assignments and other materials can be downloaded from the
[course webpage](http://rail.eecs.berkeley.edu/deeprlcourse/).
 - Lecture videos: [YouTube](https://www.youtube.com/playlist?list=PLkFD6_40KJIxJMR-j5A1mkxK26gh_qg37)
 - Syllabus and notes: [UC Berkeley CS294-112](http://rail.eecs.berkeley.edu/deeprlcourse/syllabus/)
 - Discussion: [Subreddit forum](https://www.reddit.com/r/berkeleydeeprlcourse/)


## Homeworks

There will be five homeworks. For each homework, we will post a PDF with instructions and starter code on [GitHub](https://github.com/berkeleydeeprlcourse/homework).

 - [ ] Homework 1: Imitation learning [`hw1.pdf`](hw1/instructions.pdf)
 - [ ] Homework 2: Basic (shallow) RL [`hw2.pdf`](hw2/instructions.pdf)
 - [ ] Homework 3: Deep Q Learning [`hw3.pdf`](hw3/instructions.pdf)
 - [ ] Homework 4: Deep policy gradients [`hw4.pdf`](hw4/instructions.pdf)
 - [ ] Homework 5: Research projects [`hw5a.pdf`](hw5/exploration/instructions.pdf), [`hw5c.pdf`](hw5/meta_learning/instructions.pdf), [`hw5c.pdf`](hw5/soft_actor_critic/instructions.pdf)


## Prerequisites

[CS189](https://people.eecs.berkeley.edu/~jrs/189/) or equivalent is a prerequisite for the course. This course will assume some familiarity with reinforcement learning, numerical optimization, and machine learning. For introductory material on RL and MDPs, see the [CS188 EdX course](http://ai.berkeley.edu/home.html), starting with Markov Decision Processes I, as well as Chapters 3 and 4 of [Sutton & Barto](https://web.stanford.edu/class/psych209/Readings/SuttonBartoIPRLBook2ndEd.pdf).


## Description

This course provides an introduction to theoretical foundations, algorithms, and methodologies for machine learning, emphasizing the role of probability and optimization and exploring a variety of real-world applications. Students are expected to have a solid foundation in calculus and linear algebra as well as exposure to the basic tools of logic and probability, and should be familiar with at least one modern, high-level programming language.
