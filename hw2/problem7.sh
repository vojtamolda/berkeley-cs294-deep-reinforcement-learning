#!/usr/bin/env bash

rm -rf ./data/*
python3 train_pg_f18.py LunarLanderContinuousCS294-v2 -ep=1000 --discount=0.99 -n=100 -e=3 -l=2 -s=64 \
        -b=40000 -lr=0.005 --reward_to_go --nn_baseline --exp_name nn_baseline_verify

python3 plot.py ./data/ --value=AverageReturn
