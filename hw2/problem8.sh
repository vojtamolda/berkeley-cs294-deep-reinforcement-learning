#!/usr/bin/env bash

rm -rf ./data/*

for lr in 0.005 0.01 0.02; do
  for bs in 10000 30000 50000; do
    python3 train_pg_f18.py RoboschoolHalfCheetah-v1 -ep=150 --discount=0.9 -n=100 -e=3 -l=2 -s=32 \
            -b=${bs} -lr=${lr} --reward_to_go --nn_baseline --exp_name=b${bs}_r${lr}
  done
done

python3 plot.py ./data/ --value=AverageReturn
