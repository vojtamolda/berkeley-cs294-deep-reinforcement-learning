#!/usr/bin/env bash

set -eux

envs="RoboschoolAnt-v1 RoboschoolHalfCheetah-v1 RoboschoolHopper-v1 \
      RoboschoolHumanoid-v1 RoboschoolReacher-v1 RoboschoolWalker2d-v1"

for env in ${envs}; do
    case ${1} in
         "expert_demo")
            # Showcase a rollout of expert policy
            python3 run_expert.py ${env} --render
            ;;

        "expert_rollout")
            # Collect datasets of expert policy rollouts
            python3 run_expert.py ${env} experts/${env}.rollout --num_rollouts=100 &
            ;;

        "clone_train")
            # Train a clone on expert policy rollout dataset
            python3 train_clone.py ${env} experts/${env}.rollout --num_epochs=100 --lr=0.001 &
            ;;

        "clone_demo")
            # Showcase a rollout of clone policy
            python3 run_clone.py ${env} --render
            ;;

        "clone_rollout")
            # Collect datasets of clone policy rollouts
            python3 run_clone.py ${env} clones/${env}.rollout --num_rollouts=100 &
            ;;

        "dagger_train")
            # Train a clone on expert policy with dataset aggregation
            python3 train_dagger.py ${env} --num_aggregations=50 --num_epochs=7 --lr=0.001 &
            ;;

        "dagger_demo")
            # Showcase a rollout of dagger policy
            python3 run_dagger.py ${env} --render
            ;;

        "dagger_rollout")
            # Collect datasets of dagger policy rollouts
            python3 run_dagger.py ${env} daggers/${env}.rollout --num_rollouts=100 &
            ;;

        "dagger_train")
            # Train a clone on expert policy with dataset aggregation
            python3 train_dagger.py ${env} --num_aggregations=30 --num_epochs=15 --lr=0.001 &
            ;;

        "dagger_demo")
            # Showcase a rollout of dagger policy
            python3 run_dagger.py ${env} --render
            ;;

        "dagger_rollout")
            # Collect datasets of dagger policy rollouts
            python3 run_dagger.py ${env} daggers/${env}.rollout --num_rollouts=100 &
            ;;
    esac
done
