import numpy as np
from pathlib import Path


def relu(x):
    return np.maximum(x, 0)


class Policy:
    weights_file = Path(__file__).with_suffix('.weights')
    layer_sizes = (10, 128, 64, 5)

    def __init__(self, observation_space, action_space, load_weights=True):
        assert self.layer_sizes[0] == observation_space.shape[0]
        assert self.layer_sizes[-1] == action_space.shape[0]

        self.weights_dense1_w = np.float32(self.layer_sizes[0:2])
        self.weights_dense1_b = np.float32(self.layer_sizes[1])
        self.weights_dense2_w = np.float32(self.layer_sizes[1:3])
        self.weights_dense2_b = np.float32(self.layer_sizes[2])
        self.weights_final_w = np.float32(self.layer_sizes[2:4])
        self.weights_final_b = np.float32(self.layer_sizes[3])

        if load_weights:
            self.restore()

    def act(self, ob):
        x = relu(ob @ self.weights_dense1_w + self.weights_dense1_b)
        x = relu(x @ self.weights_dense2_w + self.weights_dense2_b)
        x = x @ self.weights_final_w + self.weights_final_b
        return x

    def restore(self):
        weights = {}
        exec(self.weights_file.open().read(), weights)
        self.weights_dense1_w = weights['weights_dense1_w']
        self.weights_dense1_b = weights['weights_dense1_b']
        self.weights_dense2_w = weights['weights_dense2_w']
        self.weights_dense2_b = weights['weights_dense2_b']
        self.weights_final_w = weights['weights_final_w']
        self.weights_final_b = weights['weights_final_b']
