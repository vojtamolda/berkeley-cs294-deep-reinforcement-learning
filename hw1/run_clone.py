#!/usr/bin/env python3

import pickle
import argparse
import numpy as np

import gym
import roboschool

from train_clone import clone_map
from run_expert import execute_rollout


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Load a cloned policy and generate a dataset of rollouts.')
    parser.add_argument('env_id', type=str, metavar='roboschool-env', choices=clone_map.keys(),
                        help='roboschool environment id')
    parser.add_argument('dataset', type=str, default=None, metavar='dataset', nargs='?',
                        help='pickle file for policy rollout dataset storage')
    parser.add_argument('--num_rollouts', type=int, default=1, metavar='N',
                        help='number of episodes to rollout (default: 1)')
    parser.add_argument('--render', action='store_true',
                        help='turn on visual rendering of the environment')
    args = parser.parse_args()

    # Create gym environment and load the clone policy
    env = gym.make(args.env_id)
    clone = clone_map[args.env_id](env.observation_space, env.action_space)

    # Execute policy rollouts in the environment
    observations, actions, rewards = execute_rollout(env, clone, args.num_rollouts, args.render)
    print(f'R = {np.mean(rewards):.1f} ± {np.std(rewards):.1f}')

    # Store dataset of observations, actions and rewards
    if args.dataset:
        dataset = {'observations': observations, 'actions': actions, 'rewards': rewards}
        pickle.dump(dataset, open(args.dataset, 'wb'))
