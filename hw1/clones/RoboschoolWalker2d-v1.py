from . import Policy
from pathlib import Path


class RoboschoolWalker2d(Policy):
    weights_file = Path(__file__).with_suffix('.pytorch')
    layer_sizes = (22, 128, 64, 6)
