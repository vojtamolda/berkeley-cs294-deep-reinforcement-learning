import torch
import torch.nn as nn
from pathlib import Path


class Policy(nn.Sequential):
    weights_file = Path(__file__).with_suffix('.pytorch')
    layer_sizes = (10, 128, 64, 5)

    def __init__(self, observation_space, action_space, load_weights=True):
        assert self.layer_sizes[0] == observation_space.shape[0]
        assert self.layer_sizes[-1] == action_space.shape[0]

        layers = [
            nn.Linear(*self.layer_sizes[0:2]),
            nn.ReLU(),
            nn.Linear(*self.layer_sizes[1:3]),
            nn.ReLU(),
            nn.Linear(*self.layer_sizes[2:4])
        ]
        super().__init__(*layers)

        if load_weights:
            self.restore()

    def act(self, np_ob):
        ob = torch.from_numpy(np_ob.astype('float32'))
        act = self.forward(ob)
        return act.detach().numpy()

    def restore(self):
        if self.weights_file.exists():
            state_dict = torch.load(self.weights_file)
            self.load_state_dict(state_dict)

    def store(self):
        state_dict = self.state_dict()
        torch.save(state_dict, self.weights_file)
