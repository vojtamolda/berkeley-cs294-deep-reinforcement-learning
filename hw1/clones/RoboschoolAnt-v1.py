from . import Policy
from pathlib import Path


class RoboschoolAnt(Policy):
    weights_file = Path(__file__).with_suffix('.pytorch')
    layer_sizes = (28, 128, 64, 8)
