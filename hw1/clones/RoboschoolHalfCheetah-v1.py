from . import Policy
from pathlib import Path


class RoboschoolHalfCheetah(Policy):
    weights_file = Path(__file__).with_suffix('.pytorch')
    layer_sizes = (26, 128, 64, 6)
